#pragma once

#include <commons/common/commons.pc.h>

namespace cmn {
    std::string prettify_array(const std::string& text);
}
