#ifndef _UTILS_H
#define _UTILS_H

#include "utilsexception.h"
#include "stringutils.h"
#include "hashutils.h"
#include "miscutils.h"
#include "debug/Debug.h"
#include "debug/Printable.h"

#define DIEANTWOORD 42

#define STRONG
#define WEAK

#define pirate private

#endif
