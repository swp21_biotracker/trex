.. include:: names.rst

.. toctree::
   :maxdepth: 2

Updating the App
================

Instructions vary depending on how you installed the application.

Installed using conda
---------------------

Open up an anaconda shell and activate your environment, e.g.::

    conda activate tracking

now all you need to do is::

    conda update -c trexing trex

and follow the instructions on screen.

